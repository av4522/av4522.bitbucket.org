
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var count = 0;
window.addEventListener('load', function () {
});
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

function generacija() {
  generirajPodatke(1, function(ehrId, ime, priimek) {
    var option = document.createElement("option");
              option.text = ime + " " + priimek;
              option.value = ehrId;
              document.getElementById("dodajanjeOpcij").add(option);
    generirajPodatke(2, function(ehrId, ime, priimek) {
      var option = document.createElement("option");
              option.text = ime + " " + priimek;
              option.value = ehrId;
              document.getElementById("dodajanjeOpcij").add(option);
      generirajPodatke(3, function(ehrId, ime, priimek) {
          var option = document.createElement("option");
              option.text = ime + " " + priimek;
              option.value = ehrId;
              document.getElementById("dodajanjeOpcij").add(option);
      });
    });
  });
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta, callback) {
  var ehrId = "";
  var ime;
  var priimek;
  var datumRojstva;
  
  // idealni vitalni znaki
  if(stPacienta == 1) {
    ime = "Napoleon";
    priimek = "Vitalni";
    datumRojstva = "1990-08-15";
  }
  
  // slabi vitalni znaki - prekomerna teža in povišan krvni tlak
  else if(stPacienta == 2) {
    ime = "Willendorfska";
    priimek = "Venera";
    datumRojstva = "1980-06-06";
  }
  
  // slabi vitalni znaki - premajhna teža in prenizek krvni tlak
  else if(stPacienta == 3) {
    ime = "Valentina";
    priimek = "Kostko";
    datumRojstva = "2000-10-01";
  }
  
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              if(stPacienta == 1) {
                document.getElementById("zacetek").innerHTML = "Klikni na enega izmed <b>vzorčnih pacientov</b>: <select id='dodajanjeOpcij' onclick='zapisVPolje()'></select>";
              }
              /*var option = document.createElement("option");
              option.text = ime + " " + priimek;
              option.value = ehrId;
              document.getElementById("dodajanjeOpcij").add(option);*/
              
                
              
              
              if(stPacienta == 1) {
                console.log(ehrId);
                dodajMeritveVitalnihZnakov(stPacienta, ehrId, "2016-01-01T12:00Z", 168, 68, 120, 80);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2017-01-01T12:00Z", 169, 70, 119, 85);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2018-01-01T12:00Z", 169, 69, 125, 83);
                callback(ehrId, ime, priimek);
              }
              
              else if (stPacienta == 2) {
                console.log(ehrId);
                dodajMeritveVitalnihZnakov(stPacienta, ehrId, "2016-01-01T13:00Z", 155, 110, 200, 150);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2017-01-01T13:00Z", 160, 115, 220, 152);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2018-01-01T13:00Z", 165, 114, 250, 170);
                callback(ehrId, ime, priimek);
              }
              else if (stPacienta == 3) {
                console.log(ehrId);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2016-01-01T14:00Z", 178, 50, 100, 70);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2017-01-01T14:00Z", 180, 48, 80, 50);
                dodajMeritveVitalnihZnakov(stPacienta,ehrId, "2018-01-01T14:00Z", 184, 45, 70, 40);
                callback(ehrId, ime, priimek);
              }
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
		return ehrId;
}

function dodajMeritveVitalnihZnakov(stPacienta,ehrId, datumInUra, telesnaVisina, telesnaTeza, sistolicniKrvniTlak, diastolicniKrvniTlak) {
	
	var podatki = {
    "ctx/language": "en",
    "ctx/territory": "SI",
    "ctx/time": datumInUra,
    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
	};
	
	var parametriZahteve = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	};
	
	$.ajax({
    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      console.log("Uspešno dodane meritve za pacienta " + stPacienta);
    },
    error: function(err) {
    	console.log("Neuspešno dodane meritve za pacienta " + stPacienta);
    }
	});
	
}

function zapisVPolje() {
  var selectBox = document.getElementById("dodajanjeOpcij");
  var selectedValue = selectBox.options[selectBox.selectedIndex].value;
  document.getElementById("EHRgumb").value = selectedValue;
  osnovniPodatki();
}

function osnovniPodatki() {
  
  $("#prikaz_visine").html("");
  
  if(myChart != null) {
    myChart.destroy();
  }
  if(myChart2 != null) {
    myChart2.destroy();
  }
  
  
 
 $('#line-chart').html("");
 //$("prikaz_visine").html("<canvas id='line-chart' width='100%' height='15' align='center'></canvas>");
 $("#prikaz_teze").html("");
 $("#line-chart2").html("");
 $("#prikaz_tlaka").html("");
  var ehrId = document.getElementById("EHRgumb").value;
  
   $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
	    	  
	    	  var novo = document.getElementById("osnovniPodatki");
	    	  novo.className = "lead";
	    	  novo.align = "center";
	    	  novo.style = "border:1px solid black; padding:3%; margin: 1%; border-radius: 50px;";
  				var party = data.party;
  				$("#id").html("<p><b>Podatki za osebo:</b> " + party.firstNames + " " + party.lastNames + "<p><b>EhrId:</b> " + ehrId + "</p>");
  				
  				
  				  
  				  $.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<b>Telesna višina: </b>" + res[0].height + " " + res[0].unit;
    				    	console.log(results);
  				        $("#telesna_visina").html(results);
    			    	} else {
    			    		console.log("Ni podatkov za višino");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  					
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    		var results = "<p><b>Telesna teža:</b> " + res[0].weight + " " + res[0].unit+"</p>";
  				        $("#telesna_teza").html(results);
  				        
    			    	} else {
    			    		console.log("Ni podatkov za težo");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  						$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    		var results = "<p><b>Krvni tlak:</b> " + res[0].systolic +
                      "/" + res[0].diastolic + " " + res[0].unit +"</p>";
  				        $("#krvni_tlak").html(results);
  				        $("#hudicevGumb").html('<button type="button" onclick="prikazMeritev()">Analiziraj!</button>');
  				      // document.getElementById("hudicevGumb").style.display="block";
  				        
  				       
    			    	} else {
    			    		console.log("Ni podatkov za krvni tlak");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
	    	
		});
		
	
	
}

function prikazMeritev() {
	var ehrId = document.getElementById("EHRgumb").value;
	
	  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				var vis=[];
  				var tez=[];
  				  
  				  $.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna višina</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height +
                      " " + res[i].unit + "</td></tr>";
                      vis.push(res[i].height);
  				        }
  				        vis.reverse();
  				        prikaziGraf(vis, "višina", "line-chart");
  				        results += "</table>";
  				        $("#prikaz_visine").html(results);
    			    	} else {
    			    		console.log("Ni podatkov za višino");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  					
  					$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight +
                      " " + res[i].unit + "</td></tr>";
                      tez.push(res[i].weight);
  				        }
  				        
  				        results += "</table>";
  				        $("#prikaz_teze").html(results);
  				        tez.reverse();
  				        prikaziGraf2(tez, "teža","line-chart2");
    			    	} else {
    			    		console.log("Ni podatkov za težo");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  						$.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Krvni tlak</th></tr>";
  				        for (var i in res) {
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].systolic +
                      "/" + res[i].diastolic + " " + res[i].unit + "</td></tr>";
  				        }
  				        results += "</table>";
  				        $("#prikaz_tlaka").html(results);
    			    	} else {
    			    		console.log("Ni podatkov za krvni tlak");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
  					});
  				
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	
}

var myChart;
var myChart2;

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function prikaziGraf(data, label, chart) {
  myChart = new Chart(document.getElementById(chart), {
  type: 'line',
  data: {
    labels: [2016, 2017, 2018],
    datasets: [{ 
        data: data,
        label: label,
        borderColor: "#3e95cd",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: label + ' skozi čas'
    }
  }
});
}

function prikaziGraf2(data, label, chart) {
  myChart2 = new Chart(document.getElementById(chart), {
  type: 'line',
  data: {
    labels: [2016, 2017, 2018],
    datasets: [{ 
        data: data,
        label: label,
        borderColor: "#3e95cd",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: label + ' skozi čas'
    }
  }
});
}
function Test(){
  var weightPost = 50;
  var heightPost = 170;

	$.ajax({
    				  url: "https://www.calculator.com.my/wp-admin/admin-ajax.php",
    			    type: "POST",
    			    form: { "action": "dcube_do_calculate","to": "BMI", "converter_type": "bmic","dcube_data[Weight]": "90", "dcube_data[Height]": "1.7"},
    			    success: function (res) {
    			    	console.log("hell ye");
    			    	console.log(res);
    			    
    			    },
    			    error: function() {
    			    	console.log("Oh no");
    			    }
  					});
}