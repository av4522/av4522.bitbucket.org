/* global L, distance */

var bolnisnice;
var koordinate;
var tabela = [];

var mapa;

// za centriranje
const UKC_LAT = 46.054114;
const UKC_LNG = 14.520772;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [UKC_LAT, UKC_LNG],
    zoom: 20
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);


  // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka")
      .openOn(mapa);
    
    // različne barve glede na oddaljenost
    for(let m=0; m<tabela.length; m++) {
      
      var oddaljenost = distance(latlng.lat, latlng.lng, tabela[m]._latlngs[0][0].lat, tabela[m]._latlngs[0][0].lng, 'K');
      if(oddaljenost<1.0) {
        tabela[m].setStyle({color: 'green'}).bringToFront();
      }
      else {
        tabela[m].setStyle({color: 'blue'});
      }
    }

  }

  mapa.on('click', obKlikuNaMapo);
  
  $.getJSON("knjiznice/json/bolnisnice.json", function(json) {
  
  bolnisnice = json.features;
  
  
  // zamenjaj vrstni red lnglengs v latlngs
  for(let i = 0; i<bolnisnice.length; i++) {
    if(bolnisnice[i].geometry.type != "Polygon") continue;
    koordinate = bolnisnice[i].geometry;
    
    var koorlen = koordinate.coordinates.length;
    for(let j=0; j<koorlen; j++) {
      
      for(let k=0; k<koordinate.coordinates[j].length; k++) {
        koordinate.coordinates[j][k].reverse();
      }
      // izriši bolnišnico
      var polygon = L.polygon(koordinate.coordinates, {color: 'blue'});
     
      polygon.bindPopup("<div>" + bolnisnice[i].properties.name + "</div>" + "<div>" + bolnisnice[i].properties["addr:street"] + " " + bolnisnice[i].properties["addr:housenumber"] + "</div>"
      );
      tabela.push(polygon);
      tabela[i].addTo(mapa);
      
      mapa.fitBounds(tabela[i].getBounds());
    }
  
  mapa.panTo(new L.LatLng(UKC_LAT, UKC_LNG));
  }
  
});

});